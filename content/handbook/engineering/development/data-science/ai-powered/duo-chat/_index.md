---
title: Duo Chat Group
description: "The Duo Chat group is focused on developing GitLab Duo Chat capabilities, while supporting other product groups and the wider community in integrating more functionality."
aliases: /handbook/engineering/development/data-science/duo-chat
---

## Vision

The Duo Chat group is focused on developing GitLab Duo Chat capabilities, while supporting other product groups and the wider community in integrating more functionality.

### 🚀 Team Members

**Engineering Manager & Engineers**

{{< team-by-manager-slug "juan-silva" >}}

**Product, Design & Quality**

{{% stable-counterparts manager-role="Engineering Manager(.*)Duo Chat" role="Duo Chat" %}}

## 📚 Internal Processes

### UX Reviews

- Since Duo Chat doesn't have a dedicated UX person, the UX experts from the AI Framework team will assist with our UX reviews.
- It’s encouraged to include a clear screencast with each MR. Reviewers should try to reproduce changes locally or, if that’s not feasible, schedule a sync-up to collaborate effectively.
- UX reviews will be aimed for every user-facing MR, as long as capacities allow. Availability will be communicated on the MR itself if there are any changes.

## 🔗 Other Useful Links

### 📝 Dashboards (internal only)

- [Duo Chat Question Categorization](https://10az.online.tableau.com/#/site/gitlab/views/DuoCategoriesofQuestions/DuoCategory?:iid=1)
- [Chat QA Evaluation](https://gitlab.com/gitlab-org/ai-powered/ai-framework/qa-evaluation)
